//Usuage: Version 0
//Code finds div blocks with class codeblock
//It then replaces the innerHTML of the block the equivalent block
//to be formatted.
//To use: create a div block with class .block, include javascript file



//style to be attached element

let classname = "format";
const style = `
div.${classname} {  
    counter-reset: line;
}
div.${classname}>.line{
 
    white-space: pre;
    font-family: 'Roboto Mono', monospace;
}

div.${classname}>.line::before{
    counter-increment: line;
    content: counter(line);
    border-right: solid 1px;
}
div.${classname}>.line:nth-child(2n + 1){
    background-color:lightgray;
}
`
let styleElement = document.createElement("style");
styleElement.innerHTML = style;
styleElement.setAttribute("id", "codeStyle");
document.head.append(styleElement);

function process(){
    let codeformat = document.querySelectorAll("div.codeblock");
    
    for(let block of codeformat){
        console.log("Processing Block", block);
        output = "";
        let lines = block.innerHTML.split("\n");
        if(lines[0].trim() == ""){
            lines.shift();
        }
        for(let line of lines){
            output += `<div class=line>${line}</div>`;
        }
        block.classList.add(classname);
        if(block.hasAttribute("filename")){
          filename = block.getAttribute("filename");
          output = `<div class="title">${filename}</div>` + output;
        }
        block.innerHTML = output;
    }

}

process();
